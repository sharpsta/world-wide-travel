function initialise()
{	
//
//	SECTION DISPLAY AND BUTTONS
//

	//Hide all sections except for Personal Information
	$("section:not(:eq(0))").addClass("hidden");
	
	//Hide all other section headings
	$("section:not(:eq(0))").prev("div").prev("h2").addClass("hidden");
	
	//Add Previous section buttons to all sections except the first
	$("section:not(:first)").append("<input type='button' name='Previous' value='Previous Section' />");
	
	//Add Continue and Reset buttons to all sections except the last
	$("section:not(:last)").append("<input type='button' name='Continue' value='Continue' /><input type='reset' name='ResetSection' value='Reset Section' />");
	
	//Add Submit and Reset buttons to the last section
	$("section:last").append("<input type='submit' name='Submit' value='Submit' /><input type='reset' name='ResetSection' value='Reset Section' />");
	
	//Hide help boxes
	$(".help").addClass("hidden");
	
	updateProgress();

	
	
//
//	EVENT HANDLERS
//	
		
	//Show section help boxes when help icons are clicked
	$(".showhelp").click(function()
	{
		$(this).parent().next().toggleClass("hidden");
	});

	
	//Have partner radio buttons selected
	$("input[name='HavePartner']").change(function()
	{
		//Get value of currently selected radio button
		var radioValue = $("input[name='HavePartner']:checked").val();
		
		//if ($("input[name='HavePartner']").is(":checked"))
		if (radioValue == "yes")
		{
			$("#MyInformation").after("<h3 id='partnerinfoheading'>Partner's Information</h3>");
			
			//Clone the MyInformation fieldset and change its ID
			var fs = $("#MyInformation").clone();
			$(fs).attr("id", "PartnerInformation");
			$(fs).find("input[type='text']").val("").end();
			
			//Remove partner and children checkboxes and labels from clone
			$(fs).children("label[for^='Have'], .radiotext, input[type='radio']").remove();
			
			//Add 'Partner' to each label's for attribute and each field's name
			$(fs).children("label").attr("for", function()
			{
				return "Partner" + $(this).attr("for");
			});
			$(fs).children("input, select").attr("name", function()
			{
				return "Partner" + $(this).attr("name");
			});
			
			//Insert the cloned fieldset after the created h3
			$(fs).insertAfter("#partnerinfoheading");
		}
		else if (radioValue == "no")
		{
			$("#partnerinfoheading").remove();
			$("#PartnerInformation").remove();
		}
	});
	
	
	//Have children radio buttons selected
	$("input[name='HaveChildren']").change(function()
	{
		//Get value of currently selected radio button
		var radioValue = $("input[name='HaveChildren']:checked").val();
		
		//Check if Have Children checkbox is checked
		//if ($("input[name='HaveChildren']").is(":checked"))
		if (radioValue == "yes")
		{
			//Add a h3 after My Information
			$("#MyInformation").after("<h3 id='childreninfoheading'>Children Information</h3>");
			
			//Add a label and input for number of children input
			$("#childreninfoheading").after("<label for='NumChildren'>Number of Children</label><input name='NumChildren' type='number' min='1' max='20' value='1'/>");
			
			//Create a fieldset which will contain the names of each child and their genders			
			$("input[name='NumChildren']").after("<fieldset id='ChildrenInformation'></fieldset>");
			
			//Add inputs to the fieldset for the first child
			addChild(1);
			
			//Add or remove fields for children when Number of Children field is changed
			$("input[name='NumChildren']").change(function()
			{
				//How many entries for children are present
				var numEntries = $("input[name^='Child']").length / 2;

				//The current value of the Number of Children field
				var count = $("input[name='NumChildren']").val();
						
				if (numEntries <= count)
				{
					//Number of Children increased
					for (var i = 0; i < (count - numEntries); i++)
						addChild(count);
				}
				else
				{
					//Number of Children decreased
					for (var i = 0; i < (numEntries - count); i++)
						removeLastChild();
				}
			});
		}
		else if (radioValue == "no")
		{
			//Remove all Children information elements when unchecked
			$("#ChildrenInformation").remove();
			$("label[for='NumChildren'], input[name='NumChildren']").remove();
			$("#childreninfoheading").remove();
		}
	});
	
	
	//Use residential address radio buttons selected
	$("input[name='UseResAddress']").change(function()
	{
		var radioValue = $("input[name='UseResAddress']:checked").val();
		
		//if ($(this).is(":not(:checked)"))
		if (radioValue == "yes")
		{			
			//Clone the Residential Information fieldset and replace the 'Res' input names/label for attributes  with 'Postal'
			var clone = $("#ResidentialInformation").clone();
			$(clone).children("label").attr("for", function()
			{
				return $(this).attr("for").replace("Res", "Postal");
			});
			
			$(clone).children("input, select").attr("name", function()
			{
				return $(this).attr("name").replace("Res", "Postal");
			});
			
			//Add the cloned fieldset contents to the Postal Information fieldset
			$("#PostalInformation").append($(clone).html());
		}
		else if (radioValue == "no")
		{
			//Remove Postal Address fields if Use Residential Address checkbox is checked and any error messages
			$("#PostalInformation input[type!='radio'], #PostalInformation select, #PostalInformation label[for^='Postal']").remove();
			$("#PostalInformation .error").remove();
		}
	});
	
		
	//Continue button clicked
	$("input[name='Continue']").click(function()
	{
		console.clear();
		
		//Validate the preceeding section
		var valid = validateSection($(this).parent("section"));
				
		if (valid)
		{
			//If valid, hide this section, open the next section and update the progress box
			nextSection("section[class!='hidden']");
			$("progress").val($("progress").val() + 20);
			updateProgress();
		}
		else
		{
			//Dim the viewport and create a dialog box explaining that there are errors in the section
			$("body").append("<div id='dimmed'></div><div id='errordialog'><h3>Errors found.</h3><p>Please correct the indicated errors before continuing.</p><input type='button' name='CloseError' value='OK' /></div>");
			
			//Un-dim the viewport and remove the dialog box when the OK button is clicked
			$("input[name='CloseError']").click(function()
			{
				$("#dimmed, #errordialog").remove();
				window.scrollTo(0, 0);
			});
		}
	});
	
	
	//Previous section button clicked
	$("input[name='Previous']").click(function()
	{
		//Hide this section, open the previous one and update the progress box
		prevSection("section[class!='hidden']");
		$("progress").val($("progress").val() - 20);
		updateProgress();
	});
	
	
	
	
//
//	FUNCTIONS
//	
	
	
	//If user has children, add an entry for a new child if the number of children is increased
	function addChild(count)
	{
		//Clone the user's name and gender labels and fields
		var labels = $("#MyInformation label[for='Name'], #MyInformation label[for='Gender']").clone();
		$(labels).attr("for", function()
		{
			return "Child" + count + $(this).attr("for");
		});

		var fields = $("#MyInformation input[name='Name'], #MyInformation input[name='Gender']").clone();
		$(fields).attr("name", function()
		{
			return "Child" + count + $(this).attr("name");
		});

		//Clear the values of the cloned fields
		$(fields).val("").end();
		
		//Append the labels and fields to the Children Information div
		for (var i = 0; i < 2; i++)
		{						
			$("#ChildrenInformation").append($(labels).get(i));
		}
		
		for (var i = 0; i < 2; i++)
		{						
			$("#ChildrenInformation").append($(fields).get(i));
		}
	}
	
	
	//If user decrements number of children, remove the labels and inputs for the last child
	function removeLastChild()
	{
			$("#ChildrenInformation input[name$='Name']:last").remove();
			$("#ChildrenInformation input[name$='Gender']:last").remove();
			$("#ChildrenInformation label[for$='Name']:last, #ChildrenInformation label[for$='Gender']:last").remove();
	}
	
	
	//Open the next section
	function nextSection(currentSection)
	{
		var nextSection = $(currentSection).next("h2").next("div").next("section");
		
		//Hide the current section's heading and show the next section's heading
		$(currentSection).prev("div").prev("h2").addClass("hidden");
		$(nextSection).prev("div").prev("h2").removeClass("hidden");
		
		//Hide the current section and show the next one
		$(currentSection).addClass("hidden");	
		$(nextSection).removeClass("hidden");
		
		//Scroll the viewport back to the top of the page.
		window.scrollTo(0, 0);
	}
	
	
	//Open the previous section
	function prevSection(currentSection)
	{
		var prevSection = $(currentSection).prev("div").prev("h2").prev("section");
		
		$(currentSection).prev("div").prev("h2").addClass("hidden");
		$(prevSection).prev("div").prev("h2").removeClass("hidden");
		
		$(currentSection).addClass("hidden");	
		$(prevSection).removeClass("hidden");
		
		window.scrollTo(0, 0);
	}
	
	
	//Validate the inputs in a given section
	function validateSection(section)
	{
		var sectionValid = true;
		
		//Validation strings
		var nameChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- ";
		var streetAddressChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-/ ";
		
		//Clear all error messages
		$(".error").remove();
				
		//Validate all text fields against strings above
		$(section).find("input[type='text']").each(function (i)
		{
			var fieldValid = false;
			
			if ($(this).hasClass("name"))
				fieldValid = validateTextField($(this), $(this).val(), nameChars);
			else if ($(this).hasClass("streetname"))	
				fieldValid = validateTextField($(this), $(this).val(), streetAddressChars);
										
			if (!fieldValid)
				sectionValid = false;
		});
				
		//Validate checkbox fields
		$(section).find(".checkboxgroup").each(function(i)
		{
			console.log($(this).children("input[type='checkbox']:checked").length);
			
			if ($(this).children("input[type='checkbox']:checked").length == 0)
			{
				setInvalid(this);
				displayError(this, "Please select at least one option.");
				sectionValid = false;
			}
			else
				setValid(this);
		});
			
		//Validate the fields in the first section
		if (!$("section:first").hasClass("hidden"))
		{
			//Fields to validate
			var emailField = $(section).find("input[name$='Email']");
			var dobField = $(section).find("input[name='DOB']");
			var stateSelect = $(section).find("select[name$='State']");
			var postcodeField = $(section).find("input[name$='Postcode']");
			var dateMin = $(section).find("input[name='StartDate']");
			var dateMax = $(section).find("input[name='EndDate']");	
			var budgetMin = $(section).find("input[name='BudgetMin']");
			var budgetMax = $(section).find("input[name='BudgetMax']");		
			var datesValid = validateNumberRange(dateMin, dateMax);
			var budgetValid = validateNumberRange(budgetMin, budgetMax);

			//*** This was causing problems so had to comment out ***
			/*$(section).find("input[name$='Phone']")
			{
				if (!validatePhone($(section).find("input[type='tel']")))
					sectionValid = false;
			}*/
			
			//*** Not working (as of 28/11/19) (invalidates even if a date is entered)
			//*** Tested in Firefox 70 and Chrome 78 
			//Invalidate if email address format is invalid
			//$(section).find("input[name$='Email']")
			//{
			//	if (!validateEmail($(section).find("input[type='email']")))
			//		sectionValid = false;
			//}
						
			//Invalidate if DOB date selector is blank
			$(section).find("input[name='DOB']")
			{
				if (dobField.val().length < 1)
				{
					setInvalid(dobField);
					displayError(dobField, "This field cannot be left blank.");
					sectionValid = false;
				}
			}
			
			//Invalidate if State selector in address hasn't been changed
			if (stateSelect.val() == "Select")
			{
				setInvalid(stateSelect);
				displayError(stateSelect, "Please select a value from the list.");
				sectionValid = false;
			}
			else
			{
				setValid(stateSelect);
			}
			
			//Invalidate if postcode blank
			if (validateNumber(postcodeField))
				sectionValid = false;
			
			//Invalidate if start date is after end date
			if (!datesValid)
				displayError(dateMax, "Starting date must be on or before ending date.");

			//Invalidate if minimum budget is greater than maximum budget
			if (!budgetValid)
				displayError(budgetMax, "Minimum budget must be the same as or smaller than maximum budget.");

			if (!budgetValid || !datesValid)
				sectionValid = false;
		}
			
		return sectionValid;
	}
	
	
	//Validate text-based inputs (text, email, phone) using a string containing allowable characters
	function validateTextField(field, str, chars)
	{
		var valid = true;
		var i = 0;
		
		//Invalidate if field is required and left blank
		if ($(field).val() == "" && $(field).attr("required") == "required")
		{
			valid = false;
			displayError(field, "This field cannot be left blank.");
		}
		
		//Check for invalid characters and display an error message if any are found
		while (valid && i < str.length)
		{
			if (chars.indexOf(str.charAt(i)) == -1)
			{
				valid = false;
				
				if ($(field).attr("class") == "name")
					displayError(field, "This field can only contain upper and lowercase letters, spaces and hyphens.");
			}
			i++;
		}
		
		if (valid)
		{
			//Field valid, so add valid class and remove any error messages
			setValid(field);
			$(field).next(".error").remove();
		}
		else
		{			
			//Field invalid, so add invalid class
			setInvalid(field);
		}
		
		return valid;
	}
	
	
	//Validate phone numbers
	function validatePhone(field)
	{
		var chars = "1234567890 -()";
		var str = $(field).val();
		var valid = true;
		var i;
		
		while (valid && i < str.length)
		{
			if (chars.indexOf(str.charAt(i)) == -1)
			{
				valid = false;
				setInvalid(field);
				displayError(field, "This field can only contain numerals, spaces, hyphens and parentheses '(' and ')'.");
			}
			i++;
		}
	}	
	
	
	//Validate email address
	function validateEmail(field)
	{
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_.";
		var address = $(field).val();
		var valid = true;
		
		//Invalidate if field is required and blank
		if ($(field).attr("required") == "required" && address == "")
		{
			displayError(field, "This field cannot be left blank.");
			valid = false;
		}
		
		//Count how many times the at sign appears
		var atCount = 0;
		for (var i = 0; i < address.length; i++)
		{
			if (address[i] == "@")
				atCount++;
		}
		
		//Invalidate if at sign isn't found, at the beginning or end of the address or if there is more than one
		if (atCount == 1 && (address.indexOf("@") > 0 && address.indexOf("@") < address.length - 2))
		{
			var parts = address.split("@");
			
			for (var i = 0; i < parts.length; i++)
			{
				for (var j = 0; j < parts[i].length; j++)
				{
					if (chars.indexOf(parts[i].charAt(j)) == -1)
					{
						displayError(field, "Email address can only contain the characters 'A-Z' (uppercase and lowercase), '0-9', '-', '_' and '.'");	
						valid = false;
					}
				}
			}
		}
		else
		{
			displayError(field, "Email address must contain a single '@' symbol only.");
			valid = false;
		}
				
		if (valid)
			setValid(field);
		else
			setInvalid(field);
	
	}
	
	
	//Validates an input with a number type
	function validateNumber(field)
	{
		if (field.val() == "")
		{
			setInvalid(field);
			displayError(field, "This field cannot be left blank.");
			return true;
		}
		else
		{
			setValid(field);
			return false;
		}
	}
	
	
	//Validates two fields for a number/date range
	function validateNumberRange(low, high)
	{
		//Values of minimum and maximum fields
		lowVal = $(low).val();
		highVal = $(high).val();
		
		if (lowVal > highVal)
		{
			//Invalidate if minimum value is greater than maximum
			setInvalid(low);
			setInvalid(high);

			return false;
		}
		else
		{
			setValid(low);
			setValid(high);
			
			return true;
		}
	}
		

	//Display error messages for invalid fields
	function displayError(field, message)
	{
		if ($(field).next(".error").length == 0)
		{
			//Create a new error message div if there isn't one
			$(field).after("<div class='error'>" + "<p class='errortext'><img src='invalid.png' width='15' height='15' alt='Field error' />" + message + "</p>" + "</div>");
		}
		else
		{
			var found = false;
			
			//If there's already an error message div, change its message
			$(field).next(".error").find(".errortext").each(function()
			{
				if (!found)
				{
					if ($(this).text() == message)
						found = true;
					else
						$(field).next(".error").append("<p class='errortext'><img src='invalid.png' width='15' height='15' alt='Field error' />" + message + "</p>");
				}
			});
			
		}
	}
	

	//Display a green outline around a field that is valid
	function setValid(field)
	{
		$(field).removeClass("invalid");
		$(field).addClass("valid");
	}
	
	
	//Display a red outline around a field that is not valid
	function setInvalid(field)
	{
		$(field).removeClass("valid");
		$(field).addClass("invalid");
	}
	

	//Update the progress box at the top of the viewport
	function updateProgress()
	{
		var currentSection = $("section").not(".hidden");
		$("#progressbox h4").text($(currentSection).prev().prev("h2").text());
	}
}
$(initialise);