# World Wide Travel

A JQuery-based registration form system for a fictional travel company, developed as an assignment for my University course in client-side web development in late 2017.

### Bugs and known issues

* Date of birth and phone number validation in the main.js file has been commented out as they were causing problems.
   * Even if a valid date of birth was entered, the section of the form would invalidate anyway for some reason.
   * From what I remember, the DOB validation did work at the time I submitted the assignment (late October 2017), but doesn't work now (November 2019).
   * DOB validation was checked in both Firefox 70 and Chrome 78